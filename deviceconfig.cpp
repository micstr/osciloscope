#include "deviceconfig.h"
#include <utility>

DeviceConfig::DeviceConfig(QString devName, int channelNum, int channelStart,
    BDaq::ValueRange valRange, BDaq::int32 sectionLen, BDaq::int32 sectionCount,
    double clockRatePerChan, QString _profilePath)
    :
    deviceName(devName),
    channelCount(channelNum),
    channelStart(channelStart),
    valueRange(valRange),
    sectionLength(std::move(sectionLen)),
    sectionCount(sectionCount),
    clockRatePerChan(clockRatePerChan),
    profilePath(_profilePath)
{}

DeviceConfig& DeviceConfig::operator=(const DeviceConfig& dev)
{
    deviceName = dev.deviceName;
    channelCount = dev.channelCount;
    channelStart = dev.channelStart;
    valueRange = dev.valueRange;
    sectionLength = dev.sectionLength;
    sectionCount = dev.sectionCount;
    clockRatePerChan = dev.clockRatePerChan;
    profilePath = dev.profilePath;
    return *this;
}
