#ifndef DIALOGTRIGGER_H
#define DIALOGTRIGGER_H

#include <QDialog>
#include "asyncbufferaihandler.h"
#include <map>
#include "C:/Advantech/DAQNavi/Inc/bdaqctrl.h"
#include <algorithm>

using namespace Automation::BDaq;

namespace Ui {
class DialogTrigger;
}

class DialogTrigger : public QDialog
{
    Q_OBJECT

public:
    explicit DialogTrigger(AsyncBufferAIHandler* _asyncBuffer,
                           QWidget *parent = 0);
    ~DialogTrigger();

private:
    Ui::DialogTrigger *ui;
    AsyncBufferAIHandler* asyncBuffer;
    std::map<QString, TriggerAction> triggerAction{
        { "No action", ActionNone },
        { "Delay Start", DelayToStart },
        { "Delay Stop", DelayToStop }
    };
    std::map<QString, ActiveSignal> triggerEdge {
        { "Not active", ActiveNone },
        { "Rising", RisingEdge },
        { "Falling", FallingEdge },
        { "Both", BothEdge }
    };
    //std::map<QString, SignalDrop> triggerSource;
public slots:
    void addItemsToTriggerSource();
    void onAccepted();
    void onRejected();
};

#endif // DIALOGTRIGGER_H
