#ifndef OSCILOSCOPE_H
#define OSCILOSCOPE_H

#include <QtWidgets/QMainWindow>
#include "ui_osciloscope.h"
#include "graph.h"
#include "asyncbufferaihandler.h"
#include "deviceconfig.h"
#include "dialogdevice.h"
#include "dialogtrigger.h"

/*
 * This is a main application class, that is responsible for creating objects and setting interactions
 * between them. Also, it controls Widgets in GUI.
 */
class Osciloscope : public QMainWindow
{
    Q_OBJECT

public:
    Osciloscope(QWidget *parent = Q_NULLPTR);
    Graph* graph;
    AsyncBufferAIHandler* asyncBuffer;
    DeviceConfig* deviceConfig;
    DialogDevice* dialogDevice;
    DialogTrigger* dialogTrigger;
    double* dataSamples;
    size_t dataCount;
    void initialButtonsState();

private:
    Ui::OscilloscopeClass ui;

    double valueV = 2.0;
    double valueT = 1.0;

    void addEdgesToTrigger();
    inline double calculateTime();
    inline double calculateVoltage();

public slots:
    void addChannelsToTrigger();
    void setVoltageLabel();
    void setTimeLabel();
    void onStart();
    void onStop();
    void onPause();
    void onDiviceConfigured();
};

#endif // OSCILOSCOPE_H
