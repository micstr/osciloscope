#include "trigger.h"
#include <cmath>

Trigger::Trigger()
{
    level = 0.0;
    edge = Edge::NONE;
}

Trigger::Trigger(double _level, Edge _edge)
{
    level = _level;
    edge = _edge;
}

void Trigger::setLevel(double value)
{
    level = value;
}
void Trigger::setEdge(Edge _edge)
{
    edge = _edge;
}
double Trigger::getLevel()
{
    return level;
}
Trigger::Edge Trigger::getEdge()
{
    return edge;
}
bool Trigger::operator()(std::vector<double>& samples)
{
    if (level != 0.0)
        return process(samples);
    return true;
}

bool Trigger::process(std::vector<double> array)
{

    std::vector<double> temp(8);

    if (array.size() < 10)
        return true;
    for (auto it = array.begin() + 4; it != array.end() - 4; it++)
    {
        // BELOW TRIGGER LEVEL
        if ((*it < level && level > 0) || (*it >= level && level < 0))
            continue;
        // CHECK FOR EDGE DETECTION
        std::copy(it - 4, it + 4, temp.begin());
        avgFilter(temp, 0.2);
        if (edgeDetect(temp))
            return true;
    }
    return false;
}

double Trigger::avgw(double sample, double avg, double w)
{
    double res = w*sample + (1-w)*avg;
    return res;
}

void Trigger::avgFilter(std::vector<double>& vec, double w)
{
    for (size_t i = 1; i < vec.size(); i++)
        vec[i] = avgw(vec[i], vec[i-1], w);
}

bool Trigger::edgeDetect(std::vector<double>& vec)
{
    if (edge == Edge::RISING)
    {
        return isRising(vec);
    }
    else if (edge == Edge::FALLING)
    {
        return isFalling(vec);
    }
    else if (edge == Edge::BOTH)
    {
        return isRising(vec) || isFalling(vec);
    }
    else //(edge == Edge::NONE)
    {
        return true;
    }
}

bool Trigger::isRising(std::vector<double>& vec)
{
    for (size_t i = 1; i < vec.size(); i++)
    {
        if (vec[i] < vec[i-1])
            return false;
    }
    return true;
}

bool Trigger::isFalling(std::vector<double>& vec)
{
    for (size_t i = 1; i < vec.size(); i++)
    {
        if (vec[i] > vec[i-1])
            return false;
    }
    return true;
}
