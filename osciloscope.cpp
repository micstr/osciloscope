#include "osciloscope.h"
#include <QGraphicsOpacityEffect>
#include "C:/Advantech/DAQNavi/Inc/bdaqctrl.h"

using namespace Automation::BDaq;

Osciloscope::Osciloscope(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    setWindowFlags(Qt::WindowFlags(Qt::Dialog | Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint));

    dataSamples = nullptr;
    dataCount = 0;

    // Initialize objects
    deviceConfig = new DeviceConfig();
    asyncBuffer = new AsyncBufferAIHandler(deviceConfig, dataSamples, dataCount);
    dialogDevice = new DialogDevice(asyncBuffer);
    dialogTrigger = new DialogTrigger(asyncBuffer);
    graph = new Graph(ui.frame, asyncBuffer);
    graph->setFixedSize(ui.frame->size());

    // Set initial states of Widgets
    dialogTrigger->setEnabled(false);
    addEdgesToTrigger();
    initialButtonsState();
    ui.lcdNumber_V->setDigitCount(5);
    ui.lcdNumber_time->setDigitCount(5);
    ui.lcdNumber_V->display(calculateVoltage());
    ui.lcdNumber_time->display(1.0);

    // Connect SIGNALS to SLOTS
    connect(ui.pB_Start,            SIGNAL(clicked()),                   asyncBuffer,   SLOT(startRecording()));
    connect(ui.pB_Stop,             SIGNAL(clicked()),                   asyncBuffer,   SLOT(stopRecording()));
    connect(ui.pB_Start,            SIGNAL(clicked()),                   graph,         SLOT(startSlot()));
    connect(ui.pB_Stop,             SIGNAL(clicked()),                   graph,         SLOT(stopSlot()));
    connect(ui.pB_Pause,            SIGNAL(clicked()),                   graph,         SLOT(pauseSlot()));
    connect(ui.actionSearch_device, SIGNAL(triggered()),                 dialogDevice,  SLOT(exec()));
    connect(ui.pB_Trigger,          SIGNAL(clicked(bool)),               dialogTrigger, SLOT(exec()));
    connect(asyncBuffer,            SIGNAL(notifyDialogTrigger()),       dialogTrigger, SLOT(addItemsToTriggerSource()));
    connect(ui.pB_vPosMinus,        SIGNAL(clicked()),                   graph,         SLOT(moveChartLower()));
    connect(ui.pB_vPosPlus,         SIGNAL(clicked()),                   graph,         SLOT(moveChartHigher()));
    connect(ui.pB_hPosMinus,        SIGNAL(clicked()),                   graph,         SLOT(moveChartLeft()));
    connect(ui.pB_hPosPlus,         SIGNAL(clicked()),                   graph,         SLOT(moveChartRight()));
    connect(ui.pB_vScMinus,         SIGNAL(clicked()),                   graph,         SLOT(scaleChartDown()));
    connect(ui.pB_vScPlus,          SIGNAL(clicked()),                   graph,         SLOT(scaleChartUp()));
    connect(ui.pB_hScMinus,         SIGNAL(clicked()),                   graph,         SLOT(scaleChartNarrower()));
    connect(ui.pB_hScPlus,          SIGNAL(clicked()),                   graph,         SLOT(scaleChartWider()));
    connect(ui.cB_channels,         SIGNAL(currentIndexChanged(int)),    graph,         SLOT(onChannelChanged(int)));
    connect(ui.cB_channels_VerPos,  SIGNAL(currentIndexChanged(int)),    graph,         SLOT(onChannelActiveChanged(int)));
    connect(ui.cB_Edge,             SIGNAL(currentIndexChanged(int)),    graph,         SLOT(onEdgeChanged(int)));
    connect(ui.lE_Level,            SIGNAL(textChanged(const QString&)), graph,         SLOT(onLevelChanged(const QString&)));
    connect(asyncBuffer,            SIGNAL(notifyDialogTrigger()),       this,          SLOT(addChannelsToTrigger()));
    connect(ui.pB_vScMinus,         SIGNAL(clicked()),                   this,          SLOT(setVoltageLabel()));
    connect(ui.pB_vScPlus,          SIGNAL(clicked()),                   this,          SLOT(setVoltageLabel()));
    connect(ui.pB_hScMinus,         SIGNAL(clicked()),                   this,          SLOT(setTimeLabel()));
    connect(ui.pB_hScPlus,          SIGNAL(clicked()),                   this,          SLOT(setTimeLabel()));
    connect(asyncBuffer,            SIGNAL(notifyTimeLabel()),           this,          SLOT(setTimeLabel()));
    connect(ui.pB_Start,            SIGNAL(clicked()),                   this,          SLOT(onStart()));
    connect(ui.pB_Stop,             SIGNAL(clicked()),                   this,          SLOT(onStop()));
    connect(ui.pB_Pause,            SIGNAL(clicked()),                   this,          SLOT(onPause()));
    connect(asyncBuffer,            SIGNAL(notifyDialogTrigger()),       this,          SLOT(onDiviceConfigured()));
}

// SLOTS: GUI Buttons and Widgets interaction
void Osciloscope::initialButtonsState()
{
    ui.pB_Start->setEnabled(false);
    ui.pB_Stop->setEnabled(false);
    ui.pB_Pause->setEnabled(false);
}

void Osciloscope::onStart()
{
    ui.pB_Start->setEnabled(false);
    ui.pB_Stop->setEnabled(true);
    ui.pB_Pause->setEnabled(true);
}

void Osciloscope::onStop()
{
    ui.pB_Start->setEnabled(true);
    ui.pB_Stop->setEnabled(false);
    ui.pB_Pause->setEnabled(false);
}

void Osciloscope::onPause()
{
    ui.pB_Start->setEnabled(true);
    ui.pB_Stop->setEnabled(true);
    ui.pB_Pause->setEnabled(false);
}

void Osciloscope::onDiviceConfigured()
{
    ui.pB_Start->setEnabled(true);
    ui.pB_Stop->setEnabled(false);
    ui.pB_Pause->setEnabled(false);
}

void Osciloscope::addChannelsToTrigger()
{
    int channelNumber = asyncBuffer->deviceConfig->channelCount;
    ui.cB_channels->clear();
    ui.cB_channels_VerPos->clear();
    for (int i = 1; i <= channelNumber; i++)
    {
        ui.cB_channels->addItem(QString::asprintf("Channel %d", i));
        ui.cB_channels_VerPos->addItem(QString::asprintf("Channel %d", i));
    }
}

void Osciloscope::addEdgesToTrigger()
{
    ui.cB_Edge->addItem("None");
    ui.cB_Edge->addItem("Rising");
    ui.cB_Edge->addItem("Falling");
    ui.cB_Edge->addItem("Both");
}

void Osciloscope::setTimeLabel()
{
    qDebug() <<"!!!! TIME LABEL: " << calculateTime();
    double valueT = calculateTime();
    if (valueT * 1000 < 1.0)
    {
        valueT *= 1000000;
        ui.label_time_units->setText("us");
    }
    else if (valueT < 1.0)
    {
        valueT *= 1000;
        ui.label_time_units->setText("ms");
    }
    ui.lcdNumber_time->display(valueT);
}

void Osciloscope::setVoltageLabel()
{
    double valueV = calculateVoltage();
    if (valueV * 1000 < 1.0)
    {
        valueV *= 1000000;
        ui.label_V_units->setText("uV");
    }
    else if (valueT < 1.0)
    {
        valueV *= 1000;
        ui.label_V_units->setText("mV");
    }
    ui.lcdNumber_V->display(valueV);
}

double Osciloscope::calculateVoltage()
{
    return 2.0 / graph->getVertScale();
}

double Osciloscope::calculateTime()
{
    double valueT = 1.0;
    double clock = asyncBuffer->deviceConfig->clockRatePerChan;
    qDebug() << "clock: " << clock;
    qDebug() << "width: " << this->width();
    if (clock > 0.0 && clock <= 100000.0)
        valueT = graph->width() / (10 * clock * graph->getHorScale());
        //valueT = this->width() / (2 * clock * graph->getHorScale());
    return valueT;
}
