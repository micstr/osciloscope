#ifndef ASYNC_BUFFER_AI_HANDLER
#define ASYNC_BUFFER_AI_HANDLER

#include "C:/Advantech/DAQNavi/Inc/bdaqctrl.h"
#include "deviceconfig.h"
#include <vector>
#include <qdebug>

using namespace Automation;

enum TimeUnit { Nanosecond, Microsecond, Millisecond, Second };

class AsyncBufferAIHandler : public QObject
{
    Q_OBJECT
private:
    static void BDAQCALL onStoppedEvent(void* sender, BDaq::BfdAiEventArgs* args, void* userParam);
    static void BDAQCALL onDataReadyEvent(void* sender, BDaq::BfdAiEventArgs* args, void* userParam);
    static void BDAQCALL addCacheOverflowHandler(void* sender, BDaq::BfdAiEventArgs* args, void* userParam);
    static void BDAQCALL addOverrunHandler(void* sender, BDaq::BfdAiEventArgs* args, void* userParam);

public:
    AsyncBufferAIHandler(DeviceConfig* deviceDonfig, double* _samples, size_t& _dataCount);
    ~AsyncBufferAIHandler();

    DeviceConfig* deviceConfig;
    double* samples;
    BDaq::WaveformAiCtrl* waveformAiCtrl;
    TimeUnit timeUnit;
    size_t dataCount;
    void ErrorCodeCheck(BDaq::ErrorCode err, std::vector<QString>& messages);
    void ErrorCodeCheck(BDaq::ErrorCode err);
public slots:
    bool configureDevice();
    void startRecording();
    void stopRecording();

signals:
    void updateGraph(int plotsNum, int samplesNum, double period);
    void notifyGraphStop();
    void notifyDialogTrigger();
    void notifyTimeLabel();
};

#endif
