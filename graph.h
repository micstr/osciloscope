#ifndef GRAPH_H
#define GRAPH_H

#include <QtWidgets/QFrame>
#include "asyncbufferaihandler.h"
#include "trigger.h"
#include <algorithm>
#include <iterator>
#include <vector>
#include <array>
#include <map>
#include <exception>

class Graph : public QFrame
{
    Q_OBJECT
public:
    Graph(QWidget *parent, AsyncBufferAIHandler* _asyncBuffer);
    ~Graph();
    void paintEvent(QPaintEvent* event);
    AsyncBufferAIHandler* asyncBuffer;
    Trigger trigger;

    double getVertScale() { return vertScale; }
    double getHorScale() { return horScale; }
private:
    struct Plot
    {
        Plot() = default;
        ~Plot() = default;
        std::vector<double> samples;
        int xOffset = 0;
        int yOffset = 0;
        void resetOffset() { xOffset = yOffset = 0; }
        bool isActive = false;
        bool isPrimary = false;
    };
    std::vector<Plot> plots;
    std::vector<Plot> plotsPaused;

    double* samplesCurr;

    static std::array<QColor, 16> colors;
    int plotCount{};
    int samplesCountPerCh{};
    double clockRate{};
    int xIntervalMs{};
    int yIntervalV{};
    int xAxisMaxMs{};
    int yAxisMaxV{};
    int xAxisMinMs{};
    int yAxisMinV{};
    int totalSamplesNumber{};
    size_t size{};
    bool isSecondRun;

    int _height{};
    int _width{};
    int _ymid{};
    int _xmid{};
    int _xMove = 0;
    int _yMove = 0;
    double vertScale = 1.0;
    double horScale = 1.0;

    void readSamples(int _plotCount, int _samplesCountPerCh, double _clockRate);
    void writeSamplesToPlots();
    void readParamsFromBuffer(int _plotCount, int _samplesCountPerCh, double _clockRate);
    void drawLines();
    void drawChart(double* samples);
    void clear();
    int findMaxValIdx();
    void drawTest(std::vector<Plot>& plots);
    void zeroSamples();
    void setTriggeringChannel();
    void setActiveChannel();
    void moveChartVert(int move);
    bool isPaused;
    bool isStopped;
    bool isTriggered;

    int triggeringChannel = 0;
    int activeChannel = 0;

public slots:
    void draw(int _plotCount, int _samplesCountPerCh, double clockRate);
    void startSlot();
    void stopSlot();
    void pauseSlot();
    void moveChartLower() { moveChartVert(-5); update(); }
    void moveChartHigher() { moveChartVert(5); update(); }
    void moveChartLeft() { _xMove -= 5; update(); }
    void moveChartRight() { _xMove +=5; update(); }

    void scaleChartDown()   { vertScale *= 0.5; update(); }
    void scaleChartUp()     { vertScale *= 2.0; update(); }
    void scaleChartNarrower(){ horScale *= 2.0; update();}
    void scaleChartWider()  { horScale *= 0.5; update(); }

    void onChannelChanged(int channelNo);
    void onChannelActiveChanged(int channelNo);
    void onEdgeChanged(int Edge);
    void onLevelChanged(const QString& level);

};

#endif
