#ifndef DIALOGDEVICE_H
#define DIALOGDEVICE_H

#include <QDialog>
#include <QFileDialog>
#include "deviceconfig.h"
#include "C:/Advantech/DAQNavi/Inc/bdaqctrl.h"
#include <QMessageBox>
#include "asyncbufferaihandler.h"

#define TEST

using namespace Automation::BDaq;

namespace Ui {
class DialogDevice;
}

class DialogDevice : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDevice(AsyncBufferAIHandler* _asyncBuffer,
                          QWidget *parent = nullptr);
    ~DialogDevice();

private:
    Ui::DialogDevice *ui;
    AsyncBufferAIHandler* asyncBuffer;
    WaveformAiCtrl* wfaic;
    QString pathToProfile;
    std::string pathPr;

    void fillForm();
    ValueRange valRangeParse(QString vr);    
    void done(int r);
    bool validateBeforeConfig();
    void onAccepted();

signals:
    void notifyAsyncBufConfig();

public slots:
    void handleFileDialog();
};

#endif // DIALOGDEVICE_H
