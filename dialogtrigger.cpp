#include "dialogtrigger.h"
#include "ui_dialogtrigger.h"

DialogTrigger::DialogTrigger(AsyncBufferAIHandler* _asyncBuffer,
                             QWidget *parent):
    QDialog(parent),
    ui(new Ui::DialogTrigger),
    asyncBuffer(_asyncBuffer)
{
    ui->setupUi(this);
    for(auto it = triggerAction.begin(); it != triggerAction.end(); it++)
        ui->cB_Action->addItem(it->first);
    for(auto it = triggerEdge.begin(); it != triggerEdge.end(); it++)
        ui->cB_Edge->addItem(it->first);

    connect(this, SIGNAL(accepted()), this, SLOT(onAccepted()));
    connect(this, SIGNAL(rejected()), this, SLOT(onRejected()));
}

DialogTrigger::~DialogTrigger()
{
    delete ui;
}

void DialogTrigger::addItemsToTriggerSource()
{
    ui->cB_Source->clear();
    setEnabled(true);
    AiFeatures* features = asyncBuffer->waveformAiCtrl->getFeatures();
    qDebug() << "getTriggerCount:" << features->getTriggerCount();
    Array<SignalDrop>* sources = features->getTriggerSources();
    SignalPosition sigPos;
    wchar_t sigDes[128];
    if (sources != nullptr)
    {
        for (int i = 0; i < sources->getCount(); i++)
        {
            AdxGetSignalConnectionInformation(sources->getItem(i),
                                              sizeof(sigDes),
                                              sigDes,
                                              &sigPos);
            ui->cB_Source->addItem(QString::fromWCharArray(sigDes));
        }
    }
}

void DialogTrigger::onAccepted()
{
    AiFeatures* features = asyncBuffer->waveformAiCtrl->getFeatures();
    Array<SignalDrop>* sources = features->getTriggerSources();
    ErrorCode err;
    double level = ui->lE_Level->text().toDouble();
    int delay = ui->lE_Delay->text().toInt();
    err = asyncBuffer->waveformAiCtrl->getTrigger()->setAction(triggerAction[ui->cB_Action->currentText()]);
    asyncBuffer->ErrorCodeCheck(err);
    qDebug() << "SetAction:" << asyncBuffer->waveformAiCtrl->getTrigger()->getAction();
    err = asyncBuffer->waveformAiCtrl->getTrigger()->setEdge(triggerEdge[ui->cB_Edge->currentText()]);
    asyncBuffer->ErrorCodeCheck(err);
    qDebug() << "SetEdge:" << asyncBuffer->waveformAiCtrl->getTrigger()->getEdge();
    err = asyncBuffer->waveformAiCtrl->getTrigger()->setSource(
                sources->getItem(ui->cB_Source->currentIndex()));
    asyncBuffer->ErrorCodeCheck(err);
    qDebug() << "SetSource:" << asyncBuffer->waveformAiCtrl->getTrigger()->getSource();

    err = asyncBuffer->waveformAiCtrl->getTrigger()->setLevel(level);
    asyncBuffer->ErrorCodeCheck(err);
    qDebug() << "SetLevel:"  << asyncBuffer->waveformAiCtrl->getTrigger()->getLevel();
    err = asyncBuffer->waveformAiCtrl->getTrigger()->setDelayCount(delay);
    asyncBuffer->ErrorCodeCheck(err);
    qDebug() << "SetDelay" << asyncBuffer->waveformAiCtrl->getTrigger()->getDelayCount();
    err = asyncBuffer->waveformAiCtrl->Prepare();
    asyncBuffer->ErrorCodeCheck(err);
    qDebug() << "Prepared.";
    }

void DialogTrigger::onRejected()
{
    qDebug() << "DialogTrigger: rejected";
}
