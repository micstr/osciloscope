#include "asyncbufferaihandler.h"
#include <qdebug.h>
#include <iostream>
#include <qmessagebox.h>
#include <sstream>

AsyncBufferAIHandler::AsyncBufferAIHandler(DeviceConfig* _deviceConfig,
                                   double* _samples, size_t& _dataCount)
    :
    samples(_samples),
    dataCount(_dataCount)
{
    deviceConfig = _deviceConfig;
    timeUnit = Millisecond;
    waveformAiCtrl = BDaq::WaveformAiCtrl::Create();
    waveformAiCtrl->addStoppedHandler(onStoppedEvent, this);
    waveformAiCtrl->addDataReadyHandler(onDataReadyEvent,this);
    waveformAiCtrl->addCacheOverflowHandler(onDataReadyEvent,this);
    waveformAiCtrl->addOverrunHandler(onDataReadyEvent,this);
}

AsyncBufferAIHandler::~AsyncBufferAIHandler()
{
    if (samples)
    {
        delete[] samples;
        samples = nullptr;
    }
    waveformAiCtrl->BDaq::WaveformAiCtrl::Dispose();
}

bool AsyncBufferAIHandler::configureDevice()
{
    bool result = false;
    if (samples)
    {
        delete[] samples;
        samples = nullptr;
    }
    BDaq::Array<BDaq::DeviceTreeNode> *supportedDevice = waveformAiCtrl->getSupportedDevices();


    if (supportedDevice->getCount() == 0)
    {
        qDebug() << ("No device to support the currently demonstrated function!");
    }
    else
    {
        for (int i = 0; i < supportedDevice->getCount(); i++)
        {
            std::wcout << supportedDevice->getItem(i).DeviceNumber << " --- " <<
                          supportedDevice->getItem(i).Description << " --- " <<
                          supportedDevice->getItem(i).ModulesIndex << " --- " << std::endl;
        }
    }
    if (deviceConfig->sectionCount != 0)
    {
        dataCount = deviceConfig->channelCount  *
                    deviceConfig->sectionLength *
                    deviceConfig->sectionCount;
    } else {
        dataCount = deviceConfig->channelCount  *
                    deviceConfig->sectionLength;
    }
    samples = new double[dataCount];

    BDaq::ErrorCode err;
    std::vector<QString> messages;

    std::wstring desc = deviceConfig->deviceName.toStdWString();
    BDaq::DeviceInformation selDev(desc.data());

    err = waveformAiCtrl->setSelectedDevice(selDev);
    ErrorCodeCheck(err, messages);
    err = waveformAiCtrl->LoadProfile(static_cast<const wchar_t*>(deviceConfig->profilePath.toStdWString().data()));
    ErrorCodeCheck(err, messages);
    err = waveformAiCtrl->getConversion()->setChannelCount(deviceConfig->channelCount);
    ErrorCodeCheck(err, messages);
    err = waveformAiCtrl->getConversion()->setChannelStart(deviceConfig->channelStart);
    ErrorCodeCheck(err, messages);
    err = waveformAiCtrl->getConversion()->setClockRate(deviceConfig->clockRatePerChan);
    ErrorCodeCheck(err, messages);
    err = waveformAiCtrl->getRecord()->setSectionLength(deviceConfig->sectionLength);
    ErrorCodeCheck(err, messages);
    err = waveformAiCtrl->getRecord()->setSectionCount(deviceConfig->sectionCount);
    ErrorCodeCheck(err, messages);
    for (int i = 0; i < waveformAiCtrl->getChannels()->getCount(); i++)
    {
        err = waveformAiCtrl->getChannels()->getItem(i).setValueRange(deviceConfig->valueRange);
        ErrorCodeCheck(err, messages);
    }

    if (messages.empty())
    {
        result = true;
        err = waveformAiCtrl->Prepare();
        emit notifyDialogTrigger();
        emit notifyTimeLabel();
    }
    else
    {
        QMessageBox box;
        QString text;
        std::for_each(messages.begin(), messages.end(), [&text] (QString& message) {
            text += message + "\n";
        });
        box.setText(text);
        box.exec();
    }
    return result;
}


void AsyncBufferAIHandler::startRecording()
{
    BDaq::ErrorCode err;
    err = waveformAiCtrl->Start();
    ErrorCodeCheck(err);
}

void AsyncBufferAIHandler::stopRecording()
{
    BDaq::ErrorCode err;
    err = waveformAiCtrl->Stop();
    ErrorCodeCheck(err);
}

void AsyncBufferAIHandler::ErrorCodeCheck(BDaq::ErrorCode err, std::vector<QString>& messages)
{
    if (BioFailed(err))
    {
        QString message = "Sorry, there are some errors occurred, Error Code: 0x" +
            QString::number(err, 16).right(8).toUpper();
        auto it = find(messages.begin(), messages.end(), message);
        if (it == messages.end())
            messages.push_back(message);
        qDebug() << message;
    }
}

void AsyncBufferAIHandler::ErrorCodeCheck(BDaq::ErrorCode err)
{
    if (BioFailed(err))
    {
        QString message = "Sorry, there are some errors occurred, Error Code: 0x" +
            QString::number(err, 16).right(8).toUpper();
        qDebug() << message;
    }
}

void AsyncBufferAIHandler::onDataReadyEvent(void* sender, BDaq::BfdAiEventArgs* args, void* userParam)
{
    auto userParams = static_cast<AsyncBufferAIHandler*>(userParam);
    BDaq::ErrorCode err;
    if (args->Count >= userParams->dataCount)
        err = static_cast<BDaq::WaveformAiCtrl*>(sender)->
            GetData(userParams->dataCount, userParams->samples, 0, nullptr, nullptr, nullptr, nullptr);
    if (err != BDaq::Success && err != BDaq::WarningRecordEnd)
    {
        qDebug() << "++++AsyncBuffer onDataReadyEvent: Error occured!"
                    "Sorry, there are some errors occurred, Error Code: 0x" +
                    QString::number(err, 16).right(8).toUpper();
    }
    emit userParams->updateGraph(userParams->deviceConfig->channelCount,
                                 userParams->dataCount / userParams->deviceConfig->channelCount,
                                 userParams->deviceConfig->clockRatePerChan);
}


void AsyncBufferAIHandler::onStoppedEvent(void* sender, BDaq::BfdAiEventArgs* args, void* userParam)
{
    auto userParams = static_cast<AsyncBufferAIHandler*>(userParam);
    emit userParams->notifyGraphStop();
}

void AsyncBufferAIHandler::addCacheOverflowHandler(void* sender, BDaq::BfdAiEventArgs* args, void* userParam)
{
}
void AsyncBufferAIHandler::addOverrunHandler(void* sender, BDaq::BfdAiEventArgs* args, void* userParam)
{
}
