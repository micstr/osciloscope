#ifndef TRIGGER_H
#define TRIGGER_H

#include <vector>

class Trigger
{
public:
    enum class Edge { NONE, RISING, FALLING, BOTH };
    Trigger();
    Trigger(double _level, Edge edge);
    void setLevel(double value);
    void setEdge(Edge _edge);
    double getLevel();
    Edge getEdge();
    bool operator()(std::vector<double>& samlpes);

private:
    double level;
    Edge edge;

    double avgw(double sample, double avg, double w);
    void avgFilter(std::vector<double>& vec, double w);
    bool process(std::vector<double> array);
    bool edgeDetect(std::vector<double>& vec);
    bool isRising(std::vector<double>& vec);
    bool isFalling(std::vector<double>& vec);

};

#endif // TRIGGER_H
