#ifndef DEVICE_CONFIG
#define DEVICE_CONFIG

#include <QString>
#include "C:/Advantech/DAQNavi/Inc/bdaqctrl.h"

using namespace Automation;

class DeviceConfig
{
public:
    QString deviceName;
    int channelCount;
    int channelStart;
    BDaq::ValueRange valueRange;
    BDaq::int32	sectionLength;
    BDaq::int32	sectionCount;
    double clockRatePerChan;
    QString profilePath;

    ~DeviceConfig() = default;
    DeviceConfig() = default;
    DeviceConfig(QString devName, int channelNum, int channelStart,
        BDaq::ValueRange valRange, BDaq::int32 sectionLen, BDaq::int32 sectionCount,
        double clockRatePerChan, QString _profilePath);

    DeviceConfig& operator=(const DeviceConfig& dev);

};

#endif
