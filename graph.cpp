#include "graph.h"
#include <QPainter>
#include <QMessageBox>

//#define TEST

Graph::Graph(QWidget* parent, AsyncBufferAIHandler* _asyncBuffer) :
    QFrame(parent),
    asyncBuffer(_asyncBuffer)
{
    // Connections
    connect(asyncBuffer, SIGNAL(updateGraph(int, int, double)), this, SLOT(draw(int, int, double)));
    connect(asyncBuffer, SIGNAL(notifyGraphStop()), this, SLOT(stopSlot()));

    setPalette(QPalette(Qt::black));
    setAutoFillBackground(true);

    samplesCurr = nullptr;

    isSecondRun = false;
    isPaused = false;
    isStopped = true;
    isTriggered = false;

    update();
}


Graph::~Graph()
{
    clear();
}


void Graph::draw(int _plotCount, int _samplesCountPerCh, double _clockRate)
{
    readSamples(_plotCount, _samplesCountPerCh, _clockRate);
    update();
}

void Graph::paintEvent(QPaintEvent* event)
{
    _height = height();
    _width = width();
    qDebug() << "H:" << _height << "W:" << _width;
    _ymid = static_cast<int>(_height/2.0);
    _xmid = static_cast<int>(_width/2.0);
    drawLines();
    if (!isStopped)
    {
        if( !isPaused)
            //drawChart(samplesGraph);
            drawTest(plots);
        else
            //drawChart(samplesGraph);
            drawTest(plotsPaused);
    }
}

void Graph::drawLines()
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::gray, 1, Qt::DashLine, Qt::RoundCap));
    // Minor grid lines
    for (int i = _width / 10; i < static_cast<int>(_width); i += _width / 10)
    {
        painter.drawLine(i, 0, i, static_cast<int>(_height));
    }
    for (int i = _height / 10; i < static_cast<int>(_height); i += _height/10)
        painter.drawLine(0, i, static_cast<int>(_width), i);
    // Major grid lines
    painter.setPen(QPen(Qt::gray, 1, Qt::SolidLine, Qt::RoundCap));
    painter.drawLine(_width / 2, 0, _width / 2, static_cast<int>(_height));
    painter.drawLine(0, _height / 2, static_cast<int>(_width), _height / 2);

}

void Graph::drawChart(double* samples)
{
}

void Graph::readSamples(int _plotCount, int _samplesCountPerCh, double _clockRate)
{
    qDebug() << "----GRAPH read params: Plot count: " << _plotCount
             << "Samples/plot: " << _samplesCountPerCh
             << "Clock rate: " << _clockRate;

    plotCount = _plotCount;
    samplesCountPerCh = _samplesCountPerCh;
    clockRate = _clockRate;
    totalSamplesNumber = _plotCount * _samplesCountPerCh;

    size = static_cast<size_t>(totalSamplesNumber);
    if (samplesCurr == nullptr)
        samplesCurr = new double[static_cast<size_t>(size)];

    if (asyncBuffer->samples != nullptr)
    {
        memcpy(samplesCurr, asyncBuffer->samples,size*sizeof(double));
    }

    if (plots.empty())
    {
        plots.resize(static_cast<size_t>(plotCount));
        std::for_each(plots.begin(), plots.end(), [&](Plot& p)
        {
            try
            {
                p.samples.resize(2 * static_cast<size_t>(samplesCountPerCh));
            }
            catch (std::bad_alloc& ex)
            {
                qDebug() << "Bad alloc: " << ex.what();
            }
            std::fill(p.samples.begin(), p.samples.end(), 0);
        } );
    }

    writeSamplesToPlots();
    plots[0].isPrimary = true; //Temporary solution - add functiality to primary and active plot to move them
    setTriggeringChannel();
    setActiveChannel();

    if (!isTriggered)
    {
        auto triggerPlot = find_if(plots.begin(), plots.end(), []
                                   (const Plot& p) { return p.isPrimary; });
        auto temp = trigger(triggerPlot->samples);
        if (temp)
            isTriggered = true;
        else
            zeroSamples();
    }
}

void Graph::zeroSamples()
{
    std::for_each(plots.begin(), plots.end(), [] (Plot& p) { std::fill(p.samples.begin(), p.samples.end(), 0); });
}

void Graph::writeSamplesToPlots()
{
    size_t iP = 0;
    size_t iS = 0;
    for (int i = 0; i < totalSamplesNumber; i++)
    {
        iP = static_cast<size_t>(i % plotCount);
        plots[iP].samples[iS] = samplesCurr[i];
        if (iP == 0)
            iS++;
    }
    std::for_each(plots.begin(), plots.end(), [&](auto& p)
    {
        std::swap_ranges(p.samples.begin(), p.samples.begin() + samplesCountPerCh, p.samples.begin() + samplesCountPerCh);
    } );
}

void Graph::readParamsFromBuffer(int _plotCount, int _samplesCountPerCh, double _clockRate)
{
}

void Graph::clear()
{
    delete[] samplesCurr;
    samplesCurr = nullptr;
}

void Graph::startSlot()
{
   isPaused = false;
   isStopped = false;
   if (!plotsPaused.empty())
   {
       plotsPaused.clear();
   }
}

void Graph::pauseSlot()
{
    isPaused = true;
    plotsPaused = plots;
}

void Graph::stopSlot()
{
    isTriggered = false;
    isStopped = true;
    update();
    plots.clear();
    clear();
    isSecondRun = false;
}

int Graph::findMaxValIdx()
{
    for (size_t i = 0; i < static_cast<size_t>(plotCount); i++)
    {
        if (plots[i].isPrimary)
        {
            auto mid = plots[i].samples.begin() + totalSamplesNumber/plotCount;
            auto end = plots[i].samples.end();
            return static_cast<int>(std::distance(mid, std::max_element(mid, end)));
        }
    }
    return totalSamplesNumber/plotCount;
}

void Graph::drawTest(std::vector<Plot>& plots)
{
    //NEW IMPLEMENTATION FOR VECTORS
    int lx1, ly1, lx2, ly2, rx1, ry1, rx2, ry2;
    auto iMax = findMaxValIdx() + totalSamplesNumber/plotCount;
    auto iLeft = iMax, iRight = iMax;
    yIntervalV = static_cast<int>(_height / 20.0);

    while ((iLeft - 1) > 0 ||
           (/*(iRight + 1) < _width &&*/ (iRight + 1) < (totalSamplesNumber*2/plotCount - 1)))
    {
        QPainter painter(this);

        for (unsigned i = 0; i < plots.size(); i++)
        {
            painter.setPen(QPen(colors.at(i)));
            if (iLeft - 1 > 0)
            {
                lx1 = _xmid + static_cast<int>((iLeft - 1 - iMax) * horScale) + _xMove;
                lx2 = _xmid + static_cast<int>((iLeft - iMax) * horScale) + _xMove;
                ly1 = _ymid - static_cast<int>(plots[i].samples[iLeft - 1] * vertScale * yIntervalV + plots[i].yOffset);
                ly2 = _ymid - static_cast<int>(plots[i].samples[iLeft] * vertScale * yIntervalV + plots[i].yOffset);
                painter.drawLine(lx1, ly1, lx2, ly2);
            }
            if (iRight + 1 < (totalSamplesNumber*2/plotCount - 1) /*&& iRight + 1 < _width*/)
            {
                rx1 = _xmid + static_cast<int>((iRight - iMax) * horScale) + _xMove;
                rx2 = _xmid + static_cast<int>((iRight + 1 - iMax) * horScale) + _xMove;
                ry1 = _ymid - static_cast<int>(plots[i].samples[iRight] * vertScale * yIntervalV + plots[i].yOffset);
                ry2 = _ymid - static_cast<int>(plots[i].samples[iRight + 1] * vertScale * yIntervalV + plots[i].yOffset);
                painter.drawLine(rx1, ry1, rx2, ry2);
            }
        }
        iLeft--;
        iRight++;
        if (rx2 >= _width)
            break;
    }
}

void Graph::onChannelChanged(int channelNo)
{
    triggeringChannel = channelNo;

    qDebug() << "Trigger ch changed to " << channelNo;
}
void Graph::onEdgeChanged(int Edge)
{
    trigger.setEdge(static_cast<Trigger::Edge>(Edge));
    qDebug() << "Trigger edge changed to " << Edge;
}
void Graph::onLevelChanged(const QString& level)
{
    try
    {
        trigger.setLevel(std::stod(level.toUtf8().constData()));
        qDebug() << "Trigger edge changed to " << level;
    }
    catch (std::invalid_argument& ex)
    {
        qDebug() << "Exception: " << ex.what();
        QMessageBox box;
        box.setText("Invalid trigger level value");
        box.exec();
    }
}

void Graph::setTriggeringChannel()
{
    std::for_each(plots.begin(), plots.end(), [](Plot& p) {
        p.isPrimary = false;
    });
    plots[triggeringChannel].isPrimary = true;
}

void Graph::onChannelActiveChanged(int channelNo)
{
    activeChannel = channelNo;
}

void Graph::setActiveChannel()
{
    std::for_each(plots.begin(), plots.end(), [](Plot& p) {
        p.isActive = false;
    });
    plots[activeChannel].isActive = true;
}

void Graph::moveChartVert(int move)
{
    std::for_each(plots.begin(), plots.end(), [&move](Plot& p) {
        if (p.isActive)
            p.yOffset += move;
    });
}

std::array<QColor,16> Graph::colors = {
        QColor(255, 0, 0),
        QColor(0, 255, 0),
        QColor(185, 99, 188),
        QColor(255, 0, 255),
        QColor(0, 255, 255),
        QColor(255, 255, 0),
        QColor(155,55,255),
        QColor(255, 127, 0),
        QColor(106, 147, 219),
        QColor(209, 146, 117),
        QColor(143, 188, 143),
        QColor(245, 182, 204),
        QColor(40, 193, 164),
        QColor(165, 228, 64),
        QColor(204, 150, 53),
        QColor(236, 228, 137)
    };
