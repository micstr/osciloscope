#include "dialogdevice.h"
#include "ui_dialogdevice.h"

DialogDevice::DialogDevice(AsyncBufferAIHandler* _asyncBuffer,
                           QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDevice),
    asyncBuffer(_asyncBuffer)
{
    ui->setupUi(this);
    wfaic = WaveformAiCtrl::Create();

    //Connect signals to slots
    connect(this, SIGNAL(notifyAsyncBufConfig()), asyncBuffer, SLOT(configureDevice()));
    connect(ui->pushButton_fileDial,SIGNAL(clicked()), this, SLOT(handleFileDialog()));

    Array<DeviceTreeNode> *devices = asyncBuffer->waveformAiCtrl->getSupportedDevices();
    if (devices->getCount() == 0)
    {
        QMessageBox::information(this, tr("Warning!"), tr("No device available"));
        close();
    }
    for (int i = 0; i < devices->getCount(); i++)
    {
        DeviceTreeNode& node = devices->getItem(i);
        ui->comboBox_devices->addItem(QString::fromWCharArray(node.Description));
    }
    fillForm();
}

DialogDevice::~DialogDevice()
{
    delete ui;
}

void DialogDevice::fillForm()
{
    const QStringList chNums = {"1", "2", "3", "4", "5", "6", "7", "8"};
    const QStringList valRange = {"+/- 15 V", "+/- 10 V", "+/- 5 V","+/- 2.5 V",
                                 "+/- 1.25 V","+/- 1 V"};
    ui->comboBox_chCount->addItems(chNums);
    ui->comboBox_chStart->addItems(chNums);
    ui->comboBox_vr->addItems(valRange);
}

void DialogDevice::onAccepted()
{
    asyncBuffer->deviceConfig->deviceName = ui->comboBox_devices->currentText();
    asyncBuffer->deviceConfig->channelCount = ui->comboBox_chCount->currentText().toInt();
    asyncBuffer->deviceConfig->channelStart = ui->comboBox_chStart->currentText().toInt() - 1;
    asyncBuffer->deviceConfig->valueRange = valRangeParse(ui->comboBox_vr->currentText());
    asyncBuffer->deviceConfig->sectionLength = static_cast<int32>(ui->lineEdit_SL->text().toInt());
    asyncBuffer->deviceConfig->clockRatePerChan = ui->lineEdit_CR->text().toDouble();
    asyncBuffer->deviceConfig->sectionCount = static_cast<int32>(ui->lineEdit_SC->text().toInt());
    asyncBuffer->deviceConfig->profilePath = pathToProfile;
}

void DialogDevice::handleFileDialog()
{
    try
    {
        pathToProfile = QFileDialog::getOpenFileName(this, tr("Open Device Profile"));
    }
    catch (...)
    {
        qDebug() << "exception occured!";
    }
    pathToProfile = pathToProfile.replace("/","\\");
    qDebug() << pathToProfile;
}

ValueRange DialogDevice::valRangeParse(QString vr)
{
    ValueRange res;
    if (vr == "+/- 15 V")
        res = V_Neg15To15;
    else if (vr == "+/- 10 V")
        res = V_Neg10To10;
    else if (vr == "+/- 10 V")
        res = V_Neg10To10;
    else if (vr == "+/- 5 V")
        res = V_Neg5To5;
    else if (vr == "+/- 2.5 V")
        res = V_Neg2pt5To2pt5;
    else if (vr == "+/- 1.25 V")
        res = V_Neg1pt25To1pt25;
    else if (vr == "+/- 1 V")
        res = V_Neg1To1;
    return res;
}

void DialogDevice::done(int r)
{
    if (QDialog::Accepted == r)
    {
        onAccepted();
        if (validateBeforeConfig() == false)
            return;
        if (asyncBuffer->configureDevice() == false)
            return;
        else
        {
            QDialog::done(r);
            return;
        }
    }
    else
    {
        QDialog::done(r);
        return;
    }
}

bool DialogDevice::validateBeforeConfig()
{
    bool status = true;
    DeviceConfig* dev = asyncBuffer->deviceConfig;
    if (dev->sectionLength <= 0 || dev->sectionLength > 2000)
        status = false;
    if (dev->sectionCount < 0)
        status = false;
    if (dev->clockRatePerChan > 100000.0 || dev->clockRatePerChan < 0.1)
        status = false;
    if (status == false)
    {
        QMessageBox box;
        box.setText("Error, input incorrect: \n\n"
                    "- Section length should be in range 1:2000\n"
                    "- Clock Rate should be in range 0.1:100000.0\n"
                    "- Section count should be greater or equal 0 ( 0 - continous aquisition)");
        box.exec();
    }
    return status;
}
